package com.anangkur.testappbundle

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        openActivity(3000L)
    }

    // finish splashScreenActivity and move to another activity in a period of time.
    // the period time is set by user in miliseconds.
    // example: 3000L = 3 seconds.
    private fun openActivity(delayTime: Long){
        val handler = Handler()
        handler.postDelayed({
            startActivity(Intent(this, Class.forName("com.anangkur.list.ListMovieActivity")))
            finish()
        }, delayTime)
    }
}
