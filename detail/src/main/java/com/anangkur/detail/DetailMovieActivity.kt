package com.anangkur.detail

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.anangkur.api.model.ResultTrailer
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import com.squareup.picasso.Picasso

class DetailMovieActivity : AppCompatActivity(), DetailMovieView{

    private lateinit var toolbar: Toolbar
    private lateinit var imgFilm: ImageView
    private lateinit var txtTitleFilm: TextView
    private lateinit var txtReleaseDateFilm: TextView
    private lateinit var txtDeskripsiFilm: TextView
    private lateinit var youtubeFragment: YouTubePlayerSupportFragment

    private val detailMoviePresenter : DetailMoviePresenter = DetailMoviePresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)

        defineViews()
        setContent(getIntentImage(), getIntentTitle(), getIntentReleaseDate(), getIntentDescription())
        detailMoviePresenter.getTrailerMovie(getIntentIDMovie(), getApiKeyMovie())
    }

    // initiate all variable contain view from xml.
    private fun defineViews() {
        toolbar = findViewById(R.id.toolbar)
        setToolbar(toolbar)

        imgFilm = findViewById(R.id.img_film)
        txtTitleFilm = findViewById(R.id.txt_title_film)
        txtReleaseDateFilm = findViewById(R.id.txt_release_date_film)
        txtDeskripsiFilm = findViewById(R.id.txt_deskripsi_film)

        youtubeFragment = supportFragmentManager.findFragmentById(R.id.youtube_fragment) as YouTubePlayerSupportFragment
    }

    // set support action bar with toolbar from view.
    // show back button on actionbar.
    // hide title actionbar
    private fun setToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        supportActionBar?.title = ""
    }

    // set all data from intent to the activity.
    /*
    all data include (params):
     - image (poster)
     - title
     - release date
     - description
     */
    private fun setContent(image: String, title: String, releaseDate: String, description: String) {
        Picasso.with(this)
            .load(image)
            .into(imgFilm)
        txtTitleFilm.text = title
        txtReleaseDateFilm.text = releaseDate
        txtDeskripsiFilm.text = description
    }

    // get intent from previous activity
    private fun getIntentImage(): String {
        return intent.getStringExtra("IMAGE")
    }
    private fun getIntentTitle(): String {
        return intent.getStringExtra("TITLE")
    }
    private fun getIntentReleaseDate(): String {
        return intent.getStringExtra("RELEASE_DATE")
    }
    private fun getIntentDescription(): String {
        return intent.getStringExtra("DESCRIPTION")
    }
    private fun getIntentIDMovie(): String {
        return intent.getStringExtra("ID_MOVIE")
    }
    private fun getApiKeyMovie(): String {
        return intent.getStringExtra("API_KEY")
    }

    // initiate playback event listener, player state change listener, on fullscreen listener for youtube player.
    // - playback event listener handle event when video is play, pause, buffer or seek.
    private val playbackEventListener = object : YouTubePlayer.PlaybackEventListener {
        override fun onPlaying() {

        }

        override fun onPaused() {

        }

        override fun onStopped() {

        }

        override fun onBuffering(b: Boolean) {
            if (b) {

            }
        }

        override fun onSeekTo(i: Int) {

        }
    }
    // - player state change listener handle event when the youtube player is on load, ad start, video start, video ended, and error.
    private val playerStateChangeListener = object : YouTubePlayer.PlayerStateChangeListener {
        override fun onLoading() {

        }

        override fun onLoaded(s: String) {

        }

        override fun onAdStarted() {

        }

        override fun onVideoStarted() {

        }

        override fun onVideoEnded() {

        }

        override fun onError(errorReason: YouTubePlayer.ErrorReason) {

        }
    }
    // - on fullscreen listener handle event when user clik on fullscreen button, and rotate the screen to landscape mode.
    private val onFullscreenListener = YouTubePlayer.OnFullscreenListener { b ->
            if (b) {
                Log.d("Detail Movie Activity", "Fullscreen Youtube")
            }
        }

    // set youtube player with video from api the movie database.
    override fun setDetailMovie(resultList: List<ResultTrailer>) {
        val initializedListener = object : YouTubePlayer.OnInitializedListener {
            override fun onInitializationSuccess(provider: YouTubePlayer.Provider, youTubePlayer: YouTubePlayer, b: Boolean) {
                youTubePlayer.setPlaybackEventListener(playbackEventListener)
                youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener)
                youTubePlayer.setOnFullscreenListener(onFullscreenListener)
                if (!b) {
                    youTubePlayer.cueVideo(resultList[0].key)
                    Log.d("Detail Movie Activity", "Key: ${resultList[0].key}")
                }
            }
            override fun onInitializationFailure(
                provider: YouTubePlayer.Provider,
                youTubeInitializationResult: YouTubeInitializationResult
            ) {
                Log.d("Detail Movie Activity", "Error Playing Youtube")
            }
        }

        youtubeFragment.initialize("AIzaSyDPpOc2GpSYWESLcGhAziXMYcTwOj_rZRs", initializedListener)
    }

    // show toast when failed get data trailer from the movie database api.
    override fun onFailedGetDetailMovie(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
