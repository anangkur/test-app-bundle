package com.anangkur.detail

import com.anangkur.api.ApiHelper
import com.anangkur.api.ApiService
import com.anangkur.api.model.ResponseApiTrailerFilm
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailMoviePresenter(private val detailMovieView: DetailMovieView) {

    // get trailer from selected movie on the movie database api.
    fun getTrailerMovie(idMovie: String, apiKey: String) {
        val apiService: ApiService = ApiHelper().getFilmClient().create(ApiService::class.java)
        apiService.getDetailMovie("movie/$idMovie/videos", apiKey)
            .enqueue(object : Callback<ResponseApiTrailerFilm> {
                override fun onResponse(
                    call: Call<ResponseApiTrailerFilm>,
                    response: Response<ResponseApiTrailerFilm>
                ) {
                    // if success get data, set view with the data.
                    response.body()?.results?.let { detailMovieView.setDetailMovie(it) }
                }

                override fun onFailure(call: Call<ResponseApiTrailerFilm>, t: Throwable) {
                    // if failure get the data, send error message to view.
                    t.message?.let { detailMovieView.onFailedGetDetailMovie(it) }
                }
            })

    }
}