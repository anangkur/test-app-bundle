package com.anangkur.detail

import com.anangkur.api.model.ResultTrailer

interface DetailMovieView {
    fun setDetailMovie(resultList: List<ResultTrailer>)
    fun onFailedGetDetailMovie(message: String)
}