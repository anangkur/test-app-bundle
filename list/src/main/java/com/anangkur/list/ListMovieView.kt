package com.anangkur.list

import com.anangkur.api.model.ResultMovie

interface ListMovieView{
    fun setMovieNowPlaying(list: List<ResultMovie>)
    fun onFailureGetMovieNowPlaying(message: String)
}