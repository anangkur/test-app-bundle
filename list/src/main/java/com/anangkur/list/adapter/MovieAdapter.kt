package com.anangkur.list.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.anangkur.list.R
import com.anangkur.api.model.ResultMovie
import com.anangkur.list.adapter.MovieAdapter.ViewHolder
import com.squareup.picasso.Picasso

class MovieAdapter(private val movieList: List<ResultMovie>): RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, index: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_movie, viewGroup, false))
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, index: Int) {
        viewHolder.setMovie(movieList[index])
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val imgMovie: ImageView = itemView.findViewById(R.id.imgMovie)
        private val txtTitleMovie: TextView = itemView.findViewById(R.id.txtTitleMovie)

        fun setMovie(movie: ResultMovie){
            Picasso.with(itemView.context)
                .load("http://image.tmdb.org/t/p/w185"+movie.poster_path)
                .into(imgMovie)
            txtTitleMovie.text = movie.title

            itemView.setOnClickListener { onClickMovie(movie) }
        }

        private fun onClickMovie(movie: ResultMovie){
            val intentDetailMovie = Intent(itemView.context, Class.forName("com.anangkur.detail.DetailMovieActivity"))
            intentDetailMovie.putExtra(
                "IMAGE",
                "http://image.tmdb.org/t/p/w185" + movie.poster_path
            )
            intentDetailMovie.putExtra("TITLE", movie.title)
            intentDetailMovie.putExtra("RELEASE_DATE", movie.release_date)
            intentDetailMovie.putExtra("DESCRIPTION", movie.overview)
            intentDetailMovie.putExtra("ID_MOVIE", movie.id.toString())
            intentDetailMovie.putExtra("API_KEY", "4b9bfb0e83de2a4afb17c157ccb254f3")
            itemView.context.startActivity(intentDetailMovie)
        }
    }
}