package com.anangkur.list

import com.anangkur.api.ApiHelper
import com.anangkur.api.ApiService
import com.anangkur.api.model.ResponseApiNowPlayingMovies
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListMoviePresenter(val listMovieView: ListMovieView){

    // get now playing movies from the movie database api.
    fun getMoviesNowPlaying(apiKey: String, language: String, page: String){
        val apiService: ApiService = ApiHelper().getFilmClient().create(ApiService::class.java)
        apiService.getNowPlayingMovies(apiKey, language, page).enqueue(object : Callback<ResponseApiNowPlayingMovies> {
                override fun onResponse(call: Call<ResponseApiNowPlayingMovies>, response: Response<ResponseApiNowPlayingMovies>) {
                    // if success get data, set view with the data.
                    response.body()?.results?.let { listMovieView.setMovieNowPlaying(it) }
                }

                override fun onFailure(call: Call<ResponseApiNowPlayingMovies>, t: Throwable) {
                    // if failure get the data, send error message to view.
                    t.message?.let { listMovieView.onFailureGetMovieNowPlaying(it) }
                }
            })
    }
}