package com.anangkur.list

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.*
import android.util.Log
import android.widget.Toast
import com.anangkur.api.model.ResultMovie
import com.anangkur.list.adapter.MovieAdapter

class ListMovieActivity : AppCompatActivity(), ListMovieView {

    private lateinit var toolbar : Toolbar

    private lateinit var swipeLayout : SwipeRefreshLayout
    private lateinit var recycleMovie : RecyclerView

    private val listMoviePresenter : ListMoviePresenter = ListMoviePresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_movie)

        defineViews()
        Log.d("List Movie Activity", "Inisiate List Movie Presenter")
        listMoviePresenter.getMoviesNowPlaying("4b9bfb0e83de2a4afb17c157ccb254f3", "en-US", "1")
        swipeLayout.setOnRefreshListener { listMoviePresenter.getMoviesNowPlaying("4b9bfb0e83de2a4afb17c157ccb254f3", "en-US", "1") }
    }

    // initiate all variable contain view from xml.
    private fun defineViews(){
        toolbar = findViewById(R.id.toolbar)
        setToolbar(toolbar)
        swipeLayout = findViewById(R.id.swipeLayout)
        recycleMovie = findViewById(R.id.recycleMovie)
    }

    // set support action bar with toolbar from view.
    // show back button on actionbar.
    // hide title actionbar
    private fun setToolbar(toolbar: Toolbar){
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Test App Bundle"
    }

    // set data now playing movies from presenter.
    override fun setMovieNowPlaying(list: List<ResultMovie>) {
        Log.d("List Movie Activity", "set now playing movie")
        val movieAdapter = MovieAdapter(list)
        val layoutManagerOption = GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false)
        recycleMovie.layoutManager = layoutManagerOption
        recycleMovie.itemAnimator = DefaultItemAnimator()
        recycleMovie.adapter = movieAdapter

        swipeLayout.isRefreshing = false
    }

    // show toast contains error message from presenter.
    override fun onFailureGetMovieNowPlaying(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        Log.d("List Movie Activity", message)
    }
}
