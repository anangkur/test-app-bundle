package com.anangkur.api

import com.anangkur.api.model.ResponseApiNowPlayingMovies
import com.anangkur.api.model.ResponseApiTrailerFilm
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface ApiService{

    // get now playing movies from the movie database.
    /*
    params:
     - apiKey
     - language
     - page
     */
    @GET("movie/now_playing")
    fun getNowPlayingMovies(@Query("api_key") api_key: String, @Query("language") language: String, @Query("page") page: String): Call<ResponseApiNowPlayingMovies>

    // get trailer from selected movie.
    /*
    params:
     - apiKey
     */
    @GET
    fun getDetailMovie(@Url url: String, @Query("api_key") api_key: String): Call<ResponseApiTrailerFilm>
}