package com.anangkur.api.model

data class ResponseApiNowPlayingMovies(
    val dates: Dates,
    val page: Long,
    val results: List<ResultMovie>,
    val total_pages: Long,
    val total_results: Long
)