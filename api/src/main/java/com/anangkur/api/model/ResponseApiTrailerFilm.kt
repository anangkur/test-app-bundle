package com.anangkur.api.model

data class ResponseApiTrailerFilm(
    val id: Int,
    val results: List<ResultTrailer>
)