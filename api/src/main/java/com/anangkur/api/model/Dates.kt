package com.anangkur.api.model

data class Dates(
    val maximum: String,
    val minimum: String
)