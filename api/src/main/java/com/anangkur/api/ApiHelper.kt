package com.anangkur.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

// helper for generate Retrofit API Service class.
class ApiHelper{

    private lateinit var retrofit: Retrofit

    private val baseUrlApiMovieDB = "http://api.themoviedb.org/3/"

    fun getFilmClient(): Retrofit {
        val client = OkHttpClient.Builder()
            .build()
        retrofit = Retrofit.Builder()
            .baseUrl(baseUrlApiMovieDB)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit
    }
}